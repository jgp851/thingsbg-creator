package main;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import TBGfile.TBGfile;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 2630322136338626255L;
	
	private JPanel panel;
	private JTable table;
	private JMenuItem optionNew;
	private JMenuItem optionOpen;
	private JMenuItem optionSave;
	private JMenuItem optionSaveAs;
	private JMenuItem optionExit;
	private DefaultTableModel tableModel;
	public TBGfile tbgFile;
	private String currentFilePath;
	private String defaultFrameTitle;
	
	public static final int MINIATURE_COLUMN = 0;
	public static final int COLORING_COLUMN = 1;

	public MainFrame(String title) {
		super(title);
		defaultFrameTitle = title;
		
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("Plik");
		optionNew = new JMenuItem("Nowy");
		optionOpen = new JMenuItem("Otw�rz");
		optionSave = new JMenuItem("Zapisz");
		optionSaveAs = new JMenuItem("Zapisz jako...");
		optionExit = new JMenuItem("Zako�cz");
		optionNew.addActionListener(new NewItemListener());
		optionOpen.addActionListener(new OpenItemListener());
		optionSave.addActionListener(new SaveItemListener());
		optionSaveAs.addActionListener(new SaveAsItemListener(this));
		optionExit.addActionListener(new ExitItemListener(this));
		fileMenu.add(optionNew);
		fileMenu.add(optionOpen);
		fileMenu.add(optionSave);
		fileMenu.add(optionSaveAs);
		fileMenu.add(optionExit);
		menuBar.add(fileMenu);
		this.setJMenuBar(menuBar);
		optionSave.setEnabled(false);
		optionSaveAs.setEnabled(false);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(480, 495);
		this.setVisible(true);
	}

	// JMenuItem
	class ExitItemListener implements ActionListener {
		MainFrame frame;
		private ExitItemListener(MainFrame frame) {
			this.frame = frame;
		}
		public void actionPerformed(ActionEvent ev) {
			frame.dispose();
		}
	}
	class NewItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			createInterface();
		}
	}
	class OpenItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			createExistingFileFrame();
		}
	}
	class SaveItemListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			if(tbgFile != null && currentFilePath != null) {
				for(int i = 0; i < tableModel.getRowCount(); i++)
					tbgFile.setColoring(i, (boolean)tableModel.getValueAt(i, COLORING_COLUMN));
				tbgFile.writeFile(currentFilePath);
			}
			else
				JOptionPane.showMessageDialog(null, "B��d w zapisie pliku!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	class SaveAsItemListener implements ActionListener {
		JFrame frame;
		SaveAsItemListener(JFrame frame) {
			this.frame = frame;
		}
		public void actionPerformed(ActionEvent ev) {
			if(tbgFile != null) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(new FileNameExtensionFilter("TBG file (*.tbg)", "tbg"));
		        int returnVal = chooser.showSaveDialog(frame);
		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		        	File file = chooser.getSelectedFile();
					setCurrentFilePath(file.getPath());
					for(int i = 0; i < tableModel.getRowCount(); i++)
						tbgFile.setColoring(i, (boolean)tableModel.getValueAt(i, COLORING_COLUMN));
					tbgFile.writeFile(file.getPath() + ".tbg");
					setFrameTitle(file.getName() + ".tbg");
				}
			}
			else
				JOptionPane.showMessageDialog(null, "Nie utworzono pliku!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	// JButton
	class AddActionListener implements ActionListener {
		JFrame frame;
		AddActionListener(JFrame frame) {
			this.frame = frame;
		}
		public void actionPerformed(ActionEvent ev) {
			JFileChooser chooser = new JFileChooser();
			chooser.setFileFilter(new FileNameExtensionFilter("Image files (*.bmp, *.jpg, *.png)", "bmp", "png", "jpg"));
	        int returnVal = chooser.showOpenDialog(frame);
	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	        	File file = chooser.getSelectedFile();
				optionSaveAs.setEnabled(true);
				if(tbgFile.addThingFile(file.getPath()))
					tableModel.addRow(new Object[]{getMiniatureImage(file.getPath()), new Boolean(false)});
			}
		}
	}
	class DeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			try {
				int row = table.getSelectedRow();
				if(row != -1) {
					tableModel.removeRow(row);
					tbgFile.deleteFile(row);
				}
				else {
					if(table.getRowCount() == 0)
						throw new Exception("Tabela jest pusta!");
					else
						throw new Exception("Nie zaznaczono �adnego wpisu!");
				}
				if(table.getRowCount() == 0) {
					optionSave.setEnabled(false);
					optionSaveAs.setEnabled(false);
				}
			} catch(Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	private ImageIcon getMiniatureImage(ImageIcon icon) {
		TableColumnModel col = table.getColumnModel();
		float comWidth = col.getTotalColumnWidth() / col.getColumnCount();
		float comHeight = 100;
		float imgWidth = icon.getIconWidth();
		float imgHeight = icon.getIconHeight();
		if(imgWidth > comWidth || imgHeight > comHeight) {
			float proportions = imgWidth / imgHeight;
			imgHeight = comHeight;
			imgWidth = imgHeight * proportions;
		}
		return new ImageIcon(getScaledImage(icon.getImage(), (int)imgWidth, (int)imgHeight));
	}
	private ImageIcon getMiniatureImage(byte[] data) {
		return getMiniatureImage(new ImageIcon(data));
	}
	private ImageIcon getMiniatureImage(String path) {
		return getMiniatureImage(new ImageIcon(path));
	}
	/**
     * Resizes an image using a Graphics2D object backed by a BufferedImage.
     * @param srcImg - source image to scale
     * @param w - desired width
     * @param h - desired height
     * @return - the new resized image
     */
    private Image getScaledImage(Image srcImg, int w, int h){
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizedImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg, 0, 0, w, h, null);
        g2.dispose();
        return resizedImg;
    }
	private void createExistingFileFrame() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileFilter(new FileNameExtensionFilter("TBG file (*.tbg)", "tbg"));
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	File file = chooser.getSelectedFile();
 			createInterface();
 			createTBGfile(file.getPath());
 			initTable();
        }
	}
	private void createInterface() {
		if(table == null)
			createNewInterface();
		else
			resetInterface();
	}
	private void createNewInterface() {	
		if(tbgFile == null)
			createTBGfile(null);
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JPanel inputPanel = new JPanel();
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));
		
		JButton addButton = new JButton("Dodaj");
		addButton.addActionListener(new AddActionListener(this));
		JButton deleteButton = new JButton("Usu�");
		deleteButton.addActionListener(new DeleteActionListener());
		btnPanel.add(addButton);
		btnPanel.add(deleteButton);
		
		inputPanel.add(btnPanel);
		panel.add(inputPanel);
		createTable();
		this.getContentPane().add(BorderLayout.CENTER, panel);
		this.setVisible(true);
	}
	private void createTable() {
		tableModel = new DefaultTableModel() {
			private static final long serialVersionUID = 3395938228478457890L;
			public Class<?> getColumnClass(int column) {
				switch(column)
				{
				case MINIATURE_COLUMN: return ImageIcon.class;
				case COLORING_COLUMN: return Boolean.class;
				default: return Object.class;
				}
			}
			public boolean isCellEditable(int rowIndex, int columnIndex) {
				if(columnIndex == COLORING_COLUMN)
					return true;
				else
					return false;
			}
		};
		tableModel.addColumn("Miniatura");
		tableModel.addColumn("Koloryzowanie");
		table = new JTable(tableModel);
		table.getColumnModel().getColumn(MINIATURE_COLUMN).setCellRenderer(table.getDefaultRenderer(ImageIcon.class));
		table.setRowHeight(100);
		JScrollPane tableScroll = new JScrollPane(table);
		JPanel tablePanel = new JPanel();
		tablePanel.add(tableScroll);
		panel.add(tablePanel);
	}
	private void createTBGfile(String path) {
		if(path != null) {
			setFrameTitle(path);
			setCurrentFilePath(path);
			tbgFile = new TBGfile(path);
			optionSave.setEnabled(true);
			optionSaveAs.setEnabled(true);
		}
		else {
			setFrameTitle(null);
			setCurrentFilePath(null);
			tbgFile = new TBGfile();
			optionSave.setEnabled(false);
			optionSaveAs.setEnabled(false);
		}
	}
	private void initTable() {
		for(int i = 0; i < tbgFile.getBitmapCount(); i++)
			tableModel.addRow(new Object[]{getMiniatureImage(tbgFile.getFileData(i)) , tbgFile.getColoring(i)});
	}
	private void resetInterface() {
		currentFilePath = null;
		int rows = table.getRowCount();
		for(int i = 0; i < rows; i++)
			tableModel.removeRow(0);
		resetTBGfile();
		optionSave.setEnabled(false);
		optionSaveAs.setEnabled(false);
	}
	private void resetTBGfile() {
		int bitmaps = tbgFile.getBitmapCount();
		for(int i = 0; i < bitmaps; i++)
			tbgFile.deleteFile(0);
		setFrameTitle(null);
	}
	private void setCurrentFilePath(String path) {
		currentFilePath = path;
		if(path == null)
			optionSave.setEnabled(false);
		else
			optionSave.setEnabled(true);
	}
	private void setFrameTitle(String fileName) {
		if(fileName != null) {
			if(fileName.lastIndexOf('\\') != -1)
				fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);
			this.setTitle(defaultFrameTitle + " - " + fileName);
		}
		else
			this.setTitle(defaultFrameTitle);
	}
}
