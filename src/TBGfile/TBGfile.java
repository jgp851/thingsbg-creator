package TBGfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class TBGfile {
	private byte bitmapCount;
	ArrayList<Thing> fileList;
	
	public TBGfile() {
		fileList = new ArrayList<Thing>();
	}
	public TBGfile(String path) {
		fileList = new ArrayList<Thing>();
		readFile(path);
	}
	
	public boolean getColoring(int file) {
		return fileList.get(file).getColoring();
	}
	public byte[] getFileData(int file) {
		return fileList.get(file).getFileData();
	}
	public void setColoring(int file, boolean state) {
		fileList.get(file).setColoring(state);
	}
	public byte getBitmapCount() {
		return bitmapCount;
	}
	public boolean addThingFile(String path) {
		File file = new File(path);
		try {
			if(bitmapCount == 255)
				throw new Exception("Nie mo�na doda� wi�cej ni� 255 bitmap!");
			else {
				FileInputStream fs = new FileInputStream(file);
				DataInputStream ds = new DataInputStream(fs);
				int fileLength = ds.available();
				byte[] fileBuffer = new byte[fileLength];
				ds.read(fileBuffer);
				fileList.add(new Thing(false, fileBuffer, path.substring(path.lastIndexOf('.'))));
				bitmapCount++;
			
				ds.close();
				fs.close();
				return true;
			}
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "Plik: " + path + " nie istnieje!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "IO exception!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		return false;
	}
	
	public void readFile(String path) {
		File file = new File(path);
		try {
			FileInputStream fs = new FileInputStream(file);
			DataInputStream ds = new DataInputStream(fs);
			bitmapCount = ds.readByte();
			for(int i = 0; i < bitmapCount; i++) {
				Thing thing = new Thing();
				thing.readFile(ds);
				fileList.add(thing);
			}
			ds.close();
			fs.close();
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "Plik: " + path + " nie istnieje!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException ex) {
			if(ex.getMessage() != null)
				JOptionPane.showMessageDialog(null, "IO exception!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	public void writeFile(String path) {
		try {
			File file = new File(path);
			FileOutputStream fs = new FileOutputStream(file);
			DataOutputStream ds = new DataOutputStream(fs);
			ds.writeByte(bitmapCount);
			for(int i = 0; i < bitmapCount; i++)
				fileList.get(i).writeFile(ds);
			ds.close();
			fs.close();
		} catch (FileNotFoundException ex) {
			JOptionPane.showMessageDialog(null, "Plik: " + path + " nie istnieje!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException ex) {
			if(ex.getMessage() != null)
				JOptionPane.showMessageDialog(null, "IO exception!\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	public void deleteFile(int file) {
		fileList.remove(file);
		bitmapCount--;
	}
}
