package TBGfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import jgp.std.JGPMath;

public class Thing {
	private boolean coloring;
	private byte[] dataFile;
	private String fileType;
	public Thing() {}
	public Thing(boolean coloring, byte[] dataFile, String fileType) {
		this.coloring = coloring;
		this.dataFile = dataFile;
		this.fileType = fileType;
	}


	public boolean getColoring() {
		return coloring;
	}
	public byte[] getFileData() {
		return dataFile;
	}
	public void setColoring(boolean state) {
		coloring = state;
	}
	public void readFile(DataInputStream ds) throws IOException {
		coloring = ds.readBoolean();
		byte len = ds.readByte();
		fileType = "";
		for(byte i = 0; i < len; i++)
			fileType += (char)ds.readByte();
		int fileLength = ds.readInt();
		fileLength = JGPMath.little2big(fileLength);
		dataFile = new byte[fileLength];
		ds.read(dataFile);
	}
	public void writeFile(DataOutputStream ds) throws IOException {
		ds.writeBoolean(coloring);
		ds.writeByte(fileType.length());
		ds.writeBytes(fileType);
		ds.writeInt(JGPMath.little2big(dataFile.length));
		ds.write(dataFile);
	}
}
